module gitlab.com/milan44/gotile

go 1.15

require (
	github.com/briandowns/spinner v1.16.0
	github.com/fatih/color v1.10.0 // indirect
	github.com/mattn/go-isatty v0.0.13 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/oliamb/cutter v0.2.2
	github.com/pkg/errors v0.9.1 // indirect
	gitlab.com/milan44/goquant v0.0.0-20210813093615-1c03052f81c0
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
	golang.org/x/text v0.3.6
)
